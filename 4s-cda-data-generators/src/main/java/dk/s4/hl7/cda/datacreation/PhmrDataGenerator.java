package dk.s4.hl7.cda.datacreation;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converts PHMR specific data and header information csv files, to new csv
 * file, with all the information.
 * 
 */
public class PhmrDataGenerator extends DataGeneratorBase {

  public static void main(String[] args) {
    Logger logger = LoggerFactory.getLogger(PhmrDataGenerator.class);
    PhmrDataGenerator phmrDataGenerator = new PhmrDataGenerator();
    try {
      phmrDataGenerator.generatePhmrDocuments = true;
      // Load all properties
      phmrDataGenerator.readProperty(args);
      // first timestamp from properties file
      phmrDataGenerator.cronDateTimeGenerator = new CronDateTimeGenerator(phmrDataGenerator.phmrCron);
      phmrDataGenerator.parseCsvFile();
    } catch (IOException | URISyntaxException e) {
      logger.error("Parsing the input csv file faild", e);
    } catch (ParseException e) {
      logger.error("The cron expression for generating PHMR test document is invalid: " + phmrDataGenerator.phmrCron,
          e);
      logger.error("Please correct the cron expression in the config.properties files");
    }
  }

  @Override
  protected void parseCsvFile() throws IOException {
    String outputFileName = generatedPhmrDataFilesPath + "PhmrDataFileFor";
    final boolean IsQrd = false;
    parseCsvFile(phmrFilesPath, outputFileName, IsQrd);
  }

  /**
   * The PHMR specific data is computed when this method is called.
   * 
   * @param filesDataRecord
   *          , contains the specific PHMR data.
   * @param docId
   * @param docId
   * @return String that contains the new specific PHMR data as csv string.
   * 
   */
  protected List<String> manipulateData(CSVRecord filesDataRecord, String docId, String phmrTid) {
    String phmrType = filesDataRecord.get(CsvColumnNames.PHMR_TYPE.getColumnName());
    String phmrEnhed = filesDataRecord.get(CsvColumnNames.PHMR_ENHED.getColumnName());
    String phmrVaerdi = filesDataRecord.get(CsvColumnNames.PHMR_VAERDI.getColumnName());
    String medicalDeviceCode = filesDataRecord.get(CsvColumnNames.MEDICAL_DEVICE_CODE.getColumnName());
    String medicalDeviceDisplayName = filesDataRecord.get(CsvColumnNames.MEDICAL_DEVICE_DISPLAYNAME.getColumnName());
    String manufacturerModelName = filesDataRecord.get(CsvColumnNames.MANUFACTURER_MODEL_NAME.getColumnName());
    String phmr__maalinger_patient = filesDataRecord.get(CsvColumnNames.PHMR_MAALINGER_PATIENT.getColumnName());
    int maalingerPerPatient = Integer.parseInt(phmr__maalinger_patient);
    List<String> csvLines = new ArrayList<String>(maalingerPerPatient);
    String[] split = phmrVaerdi.split("#");

    String lowString = split[0];
    String highString = split[1];

    // We compute the phmrværdi for each measurement
    StringBuilder builder = new StringBuilder(1024);
    if (lowString.contains(",") || lowString.contains(".")) {
      lowString = lowString.replace(",", ".");
      highString = highString.replace(",", ".");
      double low = Double.parseDouble(lowString);
      double high = Double.parseDouble(highString);
      DecimalFormat formatter = new DecimalFormat("0.00", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
      for (int i = 0; i < maalingerPerPatient; i++) {
        Double randomDouble = (Math.random() * (high - low)) + low;
        phmrVaerdi = formatter.format(randomDouble);

        csvLines.add(buildCSVLine(docId, phmrTid, phmrType, phmrEnhed, phmrVaerdi, medicalDeviceCode,
            medicalDeviceDisplayName, manufacturerModelName, builder));
      }

    } else {
      int low = Integer.parseInt(split[0]);
      int high = Integer.parseInt(split[1]);
      int randomInt = (int) ((Math.random() * (high - low)) + low);
      phmrVaerdi = "" + randomInt;
      csvLines.add(buildCSVLine(docId, phmrTid, phmrType, phmrEnhed, phmrVaerdi, medicalDeviceCode,
          medicalDeviceDisplayName, manufacturerModelName, builder));
    }

    return csvLines;
  }

  private String buildCSVLine(String docId, String phmrTid, String phmrType, String phmrEnhed, String phmrVaerdi,
      String medicalDeviceCode, String medicalDeviceDisplayName, String manufacturerModelName, StringBuilder builder) {
    String objId = UUID.randomUUID().toString();
    builder.append(phmrTid).append(';');
    builder.append(phmrType).append(';');
    builder.append(phmrEnhed).append(';');
    builder.append(phmrVaerdi).append(';');
    builder.append(medicalDeviceCode).append(';');
    builder.append(medicalDeviceDisplayName).append(';');
    builder.append(manufacturerModelName).append(';');
    builder.append(docId).append(';');
    builder.append(objId).append(';');
    String tempLine = builder.toString();
    builder.setLength(0);
    return tempLine;
  }
}
