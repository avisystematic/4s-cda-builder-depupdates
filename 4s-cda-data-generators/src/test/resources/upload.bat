@ECHO off
SET ExePath=""
FOR /F "delims=" %%F IN ('dir /S /b *-jar-with*') DO SET ExePath="%%F"

IF NOT %ExePath% == "" (
    java -cp %ExePath% dk.s4.hl7.cda.upload.CDAUploader -properties ./input/upload.properties -inputfolder ./datacreation/output
)

SET ExePath= ""
