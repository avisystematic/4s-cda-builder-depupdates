package dk.s4.hl7.cda.model.qrd;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.model.CodedValue;

/**
 * Multiple choice where the user has given a list of answers from 0 to many.
 * 
 * The number of answers allowed is controlled by minimum and maximum.
 * The list of answers is set in the answers options list
 */
public class QRDMultipleChoiceResponse extends QRDResponse {
  private static final Logger logger = LoggerFactory.getLogger(QRDMultipleChoiceResponseBuilder.class);
  private List<CodedValue> answers = new ArrayList<CodedValue>();
  private int minimum; // low
  private int maximum; // high

  /**
   * "Effective Java" Builder for constructing QRDNumericResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QRDMultipleChoiceResponseBuilder
      extends QRDResponse.BaseQRDResponseBuilder<QRDMultipleChoiceResponse, QRDMultipleChoiceResponseBuilder> {
    public QRDMultipleChoiceResponseBuilder() {
    }

    private List<CodedValue> answers = new ArrayList<CodedValue>();
    private int minimum = -1; // low
    private int maximum = -1; // high

    public QRDMultipleChoiceResponseBuilder setInterval(int minimum, int maximum) {
      this.minimum = minimum;
      this.maximum = maximum;
      return this;
    }

    public QRDMultipleChoiceResponseBuilder addAnswer(String code, String codeSystem, String displayName,
        String codeSystemName) {
      answers.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
      return this;
    }

    public QRDMultipleChoiceResponse build() {
      validateBounds();
      return new QRDMultipleChoiceResponse(this);
    }

    private void validateBounds() {
      if (minimum < 0) {
        logger.warn("The minimum must be 0 or more but was: " + minimum + ". Defaulting to 0");
        minimum = 0;
      }
      if (maximum < 1) {
        logger.warn("The maximum must be 1 or more but was: " + maximum + ". Defaulting to 1");
        maximum = 1;
      }
      if (minimum > maximum) {
        logger.warn(String.format("Minimum is larger than maximum: [min:%s > max:%s]. Switching the two values",
            minimum, maximum));
        int temp = minimum;
        minimum = maximum;
        maximum = temp;
      }
    }

    @Override
    public QRDMultipleChoiceResponseBuilder getThis() {
      return this;
    }
  }

  private QRDMultipleChoiceResponse(QRDMultipleChoiceResponseBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    maximum = builder.maximum;
    answers = builder.answers;
  }

  @Override
  public String toString() {
    return getQuestion();
  }

  public void addAnswer(String code, String codeSystem, String displayName, String codeSystemName) {
    answers.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
  }

  public List<CodedValue> getAnswers() {
    return answers;
  }

  public int getMinimum() {
    return minimum;
  }

  public int getMaximum() {
    return maximum;
  }
}
