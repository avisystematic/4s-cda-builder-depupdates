package dk.s4.hl7.cda.codes;

import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.ID.IDBuilder;

// CHECKSTYLE:OFF
public class MedCom {

  public static final String ROOT_AUTHORITYNAME = "MedCom";
  public static final String ROOT_OID = "1.2.208.184";

  public static final String MESSAGECODE_DISPLAYNAME = "MedCom Message Codes";
  public static final String MEDICAL_DISPLAYNAME = "MedCom Instrument Codes";

  public static final String MESSAGECODE_OID = "1.2.208.184.100.1";
  public static final String DEVICE_OID = "1.2.208.184.100.2";
  public static final String MEDICAL_DEVICE_OID = "1.2.208.184.100.3";

  public static final String PHMR_ROOT_POD = "2.16.840.1.113883.10.20.9";
  public static final String DK_PHMR_ROOT_POD = "1.2.208.184.11.1";

  public static final String DK_APD_ROOT_OID = "1.2.208.184.14.1";

  public static final String DK_QRD_ROOT_OID = "1.2.208.184.13.1";
  public static final String DK_QRD_ROOT_OID_LEVEL = "1.2.208.184.13.1.1.1";

  public static final String DK_QFD_ROOT_OID = "1.2.208.184.12.1";
  public static final String DK_QFD_ROOT_OID_LEVEL = "1.2.208.184.12.1.1.1";

  // Reference
  public static final String DK_REFERENCE_ROOT_ID = "1.2.208.184.6.1";
  public static final String DK_REFERENCE_EXTERNAL_DOCUMENT = "1.2.208.184.5";
  public static final String DK_REFERENCE_EXTERNAL_URL = "1.2.208.184.5.3";
  // public static final String DK_REFERENCE_EXTERNAL_OBSERVATION =
  // "1.2.208.184.100.2";

  // Observation range (RED, YELLOW)
  public static final String DK_PHMR_OBSERVATION_RANGE_ROOT_OID = "1.2.208.184.11.1.2";
  public static final String DK_OBSERVATION_RANGE_RED_ALERT = "RAL";
  public static final String DK_OBSERVATION_RANGE_RED_ALERT_DISPLAYNAME = "Terapeutiske grænseværdier for RØD alarm";
  public static final String DK_OBSERVATION_RANGE_YELLOW_ALERT = "GAL";
  public static final String DK_OBSERVATION_RANGE_YELLOW_ALERT_DISPLAYNAME = "Terapeutiske grænseværdier for GUL alarm";

  // MedCom HL7 to identify the performer of a measurement
  public static final String PERFORMED_BY_CITIZEN = "POT";
  public static final String PERFORMED_BY_HEALTHCAREPROFESSIONAL = "PNT";
  public static final String PERFORMED_BY_CAREGIVER = "PCG";

  // Related displaynames
  public static final String PERFORMED_BY_CITIZEN_DISPLAYNAME = "Målt af borger";
  public static final String PERFORMED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME = "Målt af aut. sundhedsperson";
  public static final String PERFORMED_BY_CAREGIVER_DISPLAYNAME = "Målt af anden omsorgsperson";

  // MedCom codes to identify how data is provided to the data collection device
  public static final String TRANSFERRED_ELECTRONICALLY = "AUT";
  public static final String TYPED_BY_CITIZEN = "TPD";
  public static final String TYPED_BY_CITIZEN_RELATIVE = "TPR";
  public static final String TYPED_BY_HEALTHCAREPROFESSIONAL = "TPH";
  public static final String TYPED_BY_CAREGIVER = "TPC";

  // Related displaynames
  public static final String TRANSFERRED_ELECTRONICALLY_DISPLAYNAME = "Måling overført automatisk";
  public static final String TYPED_BY_CITIZEN_RELATIVE_DISPLAYNAME = "Indtastet af pårørende";
  public static final String TYPED_BY_CITIZEN_DISPLAYNAME = "Indtastet af borger";
  public static final String TYPED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME = "Indtastet af aut. sundhedsperson";
  public static final String TYPED_BY_CAREGIVER_DISPLAYNAME = "Indtastet af anden omsorgsperson";

  // Prompt table
  public static final String MEDCOM_PROMPT_OID = "1.2.208.184.100.2";
  public static final String MEDCOM_PROMPT_TABLE = "Medcom prompt table";

  public static ID createId(String Id) {
    return new IDBuilder().setRoot(ROOT_OID).setExtension(Id).setAuthorityName(ROOT_AUTHORITYNAME).build();
  }

  public static ID createURLId(String url) {
    return new IDBuilder()
        .setAuthorityName(ROOT_AUTHORITYNAME)
        .setExtension(url)
        .setRoot(DK_REFERENCE_EXTERNAL_URL)
        .build();
  }
}
