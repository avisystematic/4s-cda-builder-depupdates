package dk.s4.hl7.cda.convert.decode.qrd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class SectionHandler implements CDAXmlHandler<QRDDocument> {
  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section";

  private String title;
  private String language;
  private List<Section<QRDResponse>> sections;
  private ObservationHandler observationHandler;
  private RawTextHandler rawTextHandler;

  public SectionHandler() {
    title = null;
    language = null;
    observationHandler = new ObservationHandler();
    sections = new ArrayList<Section<QRDResponse>>();
    rawTextHandler = new RawTextHandler(COMPONENT_SECTION + "/text", "text");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "title")) {
      title = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "languageCode", "code")) {
      language = xmlElement.getAttributeValue("code");
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "section")) {
      Section<QRDResponse> section = new Section<QRDResponse>(title, rawTextHandler.getRawText(), language);
      for (QRDResponse response : observationHandler.getResponses()) {
        section.addQuestionnaireEntity(response);
      }
      sections.add(section);
      clear();
    }
  }

  public void clear() {
    rawTextHandler.clear();
    title = null;
    language = null;
    observationHandler.clear();
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    observationHandler.addHandlerToMap(xmlMapping);
    xmlMapping.add(COMPONENT_SECTION, this);
    xmlMapping.add(COMPONENT_SECTION + "/title", this);
    xmlMapping.add(COMPONENT_SECTION + "/languageCode", this);
    rawTextHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    observationHandler.removeHandlerFromMap(xmlMapping);
    rawTextHandler.removeHandlerFromMap(xmlMapping);
    xmlMapping.remove(COMPONENT_SECTION);
    xmlMapping.remove(COMPONENT_SECTION + "/title");
    xmlMapping.remove(COMPONENT_SECTION + "/languageCode");
  }

  @Override
  public void addDataToDocument(QRDDocument clinicalDocument) {
    for (Section<QRDResponse> section : sections) {
      clinicalDocument.addSection(section);
    }
  }
}
