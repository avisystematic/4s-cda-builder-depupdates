package dk.s4.hl7.cda.convert.decode.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class QuestionOptionsPatternHandler extends BaseXmlHandler {
  private static final Logger logger = LoggerFactory.getLogger(QuestionOptionsPatternHandler.class);
  public static final String OBSERVATION_BASE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/entryRelationship/observation";
  private int minimum = -1;
  private int maximum = -1;

  public QuestionOptionsPatternHandler() {
    addPath(OBSERVATION_BASE + "/value/low");
    addPath(OBSERVATION_BASE + "/value/high");
  }

  public int getMinimum() {
    return minimum;
  }

  public int getMaximum() {
    return maximum;
  }

  public boolean hasValues() {
    return minimum > -1 && maximum > -1;
  }

  public void clear() {
    minimum = -1;
    maximum = -1;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      minimum = parseToInteger(xmlElement.getAttributeValue("value"), "low");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      maximum = parseToInteger(xmlElement.getAttributeValue("value"), "high");
    }
  }

  private int parseToInteger(String value, String valueName) {
    try {
      return Integer.parseInt(value);
    } catch (Exception ex) {
      logger.warn(String.format("The value %s is not an integer: %s", valueName, value));
    }
    return -1;
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

}
