package goimplement.it;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.api.json.JSONConfiguration;

import goimplement.it.Hl7JkiddoRhcloudCom_Service;
import goimplement.it.Hl7JkiddoRhcloudCom_Service.CDA;

public final class ClientBuilder {

	public static CDA newClient()
	{
		final ClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		final Client client = Client.create(config);
		client.addFilter(new LoggingFilter());
		return Hl7JkiddoRhcloudCom_Service.cDA(client);
	}
}
